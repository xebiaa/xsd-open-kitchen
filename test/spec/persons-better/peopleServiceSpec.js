describe('PeopleService', function () {

  var peopleResource;

  beforeEach(module('myApp', function ($provide) {
    peopleResource = jasmine.createSpyObj('peopleResource', ['get', 'query']);
    $provide.value('PeopleResource', peopleResource);
  }));

  var
    $scope,
    deferred,
    persons = [
      { first: 'Frank', route: 'frank' },
      { first: 'Mike', route: 'mike' },
      { first: 'Mark', route: 'mark' }
    ],
    peopleService;


  beforeEach(inject(function ($q, $rootScope, _peopleService_) {
    $scope = $rootScope.$new();

    peopleService = _peopleService_;

    deferred = $q.defer();
    deferred.resolve(persons);
    peopleResource.query.andReturn({
      $promise: deferred.promise
    });

  }));

  describe('the getPeople method', function () {

    it('should return list of PersonResources', function () {

      var personsResult;

      peopleService.getPeople()
        .then(function (result) {
          personsResult = result;
        });

      $scope.$digest();
      expect(personsResult).toEqual(persons);

    });

    it('should return list of PersonResource (better way)', function () {
      var successCb = jasmine.createSpy('success');
      var errorCb = jasmine.createSpy('error');

      peopleService.getPeople()
        .then(successCb, errorCb);

      $scope.$digest();
      expect(successCb).toHaveBeenCalledWith(persons);
      expect(errorCb).not.toHaveBeenCalled();
    });
  });

  describe('the savePerson method', function () {
    it('should call $save on the passed in persons', function () {
      var personSpy = jasmine.createSpyObj('personSpy', ['$save']);
      peopleService.savePerson(personSpy);
      expect(personSpy.$save).toHaveBeenCalled();
    });
  });
});
