describe('peopleDirective', function () {

  beforeEach(module('myApp'));

  var
    $scope,
    element,
    people = [
      { first: 'Frank', route: 'frank' },
      { first: 'Mike', route: 'mike' },
      { first: 'Mark', route: 'mark' }
    ];

  beforeEach(inject(function ($rootScope, $compile) {
    $scope = $rootScope.$new();
    $scope.people = people;
    element = $compile('<div people-directive="people"></div>')($scope);
    $scope.$digest();
  }));

  it('should have a list of people', function () {
    expect(element.find('ul li').length).toBe(3);
    expect(element.find('ul li').eq(0).text()).toBe(people[0].first);
    expect(element.find('ul li a').eq(0).attr('href')).toBe('#/' + people[0].route);
  });

});
