describe('people.html', function() {

	var
    $compile,
    $rootScope,
		$scope,
    $templateCache,
		element,
		template = 'scripts/people/people.html',
		people = [
      { first: 'Frank', route: 'frank' },
      { first: 'Mike', route: 'mike' },
      { first: 'Mark', route: 'mark' }
    ];

  function compileTemplate(scope) {
    return $compile('<div>' + $templateCache.get(template) + '</div>')(scope);
  }

	beforeEach(function() {
    module('myApp', 'scripts/people/people.html');

    inject(function (_$compile_, _$rootScope_, _$templateCache_) {
      $compile = _$compile_;
      $rootScope = _$rootScope_;
      $templateCache = _$templateCache_;

      $scope = $rootScope.$new();
      $scope.people = people;
		  element = compileTemplate($scope);
      $scope.$digest();
    });
	});

	it('should have a list of people', function() {
		var peopleElement = element.find('ul#people li[ng-repeat]');
		expect(peopleElement.eq(0).text()).toBe('Frank - frank');
	});

	it('should have a peopleDirective', function() {
		expect(element.find('[people-directive]').attr('people-directive')).toBe('people');
	});

	describe('the link between controller and view (midway test)', function() {
		it('should have the same people on the scope', inject(function($controller, $httpBackend) {
			$httpBackend.expectGET('scripts/people/people.json').respond(people);

			var ctrlScope = $rootScope.$new();
			$controller('PeopleController', {
				$scope: ctrlScope
			});
			$httpBackend.flush();

      var midwayElement = compileTemplate(ctrlScope);
      ctrlScope.$digest();

			expect(midwayElement.find('ul#people li[ng-repeat]').length).toBe(3);
		}));
	});
});
