describe('PeopleResource', function () {

  beforeEach(module('myApp'));

  var
    $httpBackend,
    people = [
      { first: 'Frank', route: 'frank' },
      { first: 'Mike', route: 'mike' },
      { first: 'Mark', route: 'mark' }
    ],
    peopleResource;

  beforeEach(inject(function (_$httpBackend_, _PeopleResource_) {
    peopleResource = _PeopleResource_;

    $httpBackend = _$httpBackend_;

    $httpBackend.expectGET('scripts/people/people.json').respond(people);
  }));

  it('should call the url scripts/people/people.json and return an array with persons', function () {
    peopleResource.query();
    $httpBackend.flush();
  });

});
