describe('PeopleController', function () {

  var
    $scope,
    people = [
      { first: 'Frank', route: 'frank' },
      { first: 'Mike', route: 'mike' },
      { first: 'Mark', route: 'mark' }
    ],
    peopleService = jasmine.createSpyObj('PeopleService', ['getPeople', 'savePerson']);

  beforeEach(function () {
    module('myApp');
    inject(function ($controller, $rootScope, $q) {
      var deferred = $q.defer();
      deferred.resolve(people);
      peopleService.getPeople.andReturn(deferred.promise);

      $scope = $rootScope.$new();

      $controller('PeopleController', {
        $scope: $scope,
        peopleService: peopleService
      });
    });
  });

  it('should attach a list of Persons to the scope', function () {
    $scope.$digest();
    expect($scope.people).toBe(people);
  });
});
