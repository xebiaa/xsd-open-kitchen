describe('PeopleResource', function () {

  it('should call the url scripts/people/people.json and return an array with people', function () {
    mox.module('myApp').run();

    requestTest()
      .whenMethod(mox.inject('PeopleResource').query)
      .expectGet('scripts/people/people.json')
      .andRespond([])
      .run();
  });

});
