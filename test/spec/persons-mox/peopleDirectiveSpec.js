describe('peopleDirective', function () {

  var
    $scope,
    element,
    people = getMockData('people.json');

  beforeEach(function () {
    mox
      .module('myApp')
      .run();

    $scope = createScope({
      people: people
    });
    element = addSelectors(compileHtml('<div people-directive="people"></div>', $scope), {
      people: 'ul li',
      links: 'ul li a'
    });
  });

  it('should have a list of people', function () {
    expect(element.people()).toHaveLength(people.length);
    expect(element.people().eq(0)).toHaveText(people[0].first);
    expect(element.links().eq(0)).toHaveAttr('href', '#/' + people[0].route);
  });

});
