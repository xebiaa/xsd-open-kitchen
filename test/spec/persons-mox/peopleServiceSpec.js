describe('peopleService', function () {

  var people = getMockData('people.json'),
    peopleService;

  beforeEach(function () {
    mox
      .module('myApp')
      .mockServices('PeopleResource')
      .setupResults(function () {
        return {
          PeopleResource: {
            query: resourceResult(people),
            $save: promise()
          }
        };
      })
      .run();

      peopleService = mox.inject('peopleService');
  });

  describe('the getPeople method', function () {

    it('should return list of PeopleResources', function () {
      expect(peopleService.getPeople()).toHaveBeenResolvedWith(people);
    });

  });

  describe('The savePerson method', function () {
    it('should call $save on the person object passed in', function () {
      expect(peopleService.savePerson(mox.get.PeopleResource)).toResolve();
      expect(mox.get.PeopleResource.$save).toHaveBeenCalled();
    });
  });
});
