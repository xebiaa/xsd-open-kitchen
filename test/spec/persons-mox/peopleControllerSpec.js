describe('PeopleController', function () {

  var
    $log,
    $scope,
    people = getMockData('people.json');

  function initController() {
    createController('PeopleController', $scope);
    $scope.$digest();
  }

  beforeEach(function () {
    mox
      .module('myApp')
      .mockServices('peopleService')
      .setupResults(function () {
        return {
          peopleService: { getPeople: promise(people) }
        };
      })
      .run();

    $scope = createScope();
    initController();

    $log = mox.inject('$log');
    spyOn($log, 'log');
  });

  describe('on initialization', function () {
    it('should attach a list of Persons to the scope', function () {
      expect($scope.people).toEqual(people);
    });

    describe('when fetching the list of people fails', function () {
      it('should $log the message from the service', function () {
        var msg = { data: 'MISPOES!'};
        mox.get.peopleService.getPeople.andReturn(reject(msg));
        initController();
        expect($log.log).toHaveBeenCalledWith(msg.data);
      });
    });
  });
});
