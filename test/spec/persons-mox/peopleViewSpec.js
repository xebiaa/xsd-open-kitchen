describe('people.html', function() {

	var
		$scope,
		element,
		template = 'scripts/people/people.html',
		people = getMockData('people.json');

	beforeEach(function() {
		mox
			.module('myApp', template)
			.run();

		$scope = createScope({
			people: people
		});
		element = addSelectors(compileTemplate(template, $scope), {
			people: '#people li',
			peopleDirective: '[people-directive]'
		});
	});

	it('should have a list of people', function() {
		expect(element.people()).toHaveLength(3);
    expect(element.people().eq(0)).toHaveText('Frank - frank');
    expect(element.people().eq(0)).toHaveText(people[0].first + ' - ' + people[0].route);
  });

	it('should have a peopleDirective', function() {
		expect(element.peopleDirective()).toContainIsolateScope({
			people: people
		});
	});
});
