describe('PeopleController', function () {

  beforeEach(module('myApp'));

  var
    $controller,
    $httpBackend,
    $log,
    $scope,
    people = [
      { first: 'Frank', route: 'frank' },
      { first: 'Mike', route: 'mike' },
      { first: 'Mark', route: 'mark' }
    ];

  function initController() {
    $controller('PersonController', {
      $scope: $scope,
      $log: $log
    });
  }

  beforeEach(inject(function (_$controller_, _$httpBackend_, _$log_, $rootScope) {
    $controller = _$controller_;
    $httpBackend = _$httpBackend_;
    $log = _$log_;
    $scope = $rootScope.$new();
  }));

  it('should attach a list of Persons to the scope', function () {
    $httpBackend.expectGET('scripts/people/people.json').respond(people);
    initController();
    $httpBackend.flush();

    expect($scope.people.length).toBe(3);
  });

  it('should show an error in the console when something went wrong', function () {
    spyOn($log, 'log');
    var msg = 'BUMMER!';

    $httpBackend.expectGET('scripts/people/people.json').respond(404, msg);
    initController();
    $httpBackend.flush();

    expect($log.log).toHaveBeenCalledWith(msg);
  });
});
