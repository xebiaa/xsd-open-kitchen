/* jshint strict:false */

// custom matchers to help with winning at everything

// format a str, replacing {NUMBER} with the n'th argument
// and uses jasmine.pp for formatting the arguments
function format(str) {
  var message = str;
  for (var i = 1; i < arguments.length; i++) {
    message = message.replace(new RegExp('\\{' + (i - 1) + '\\}', 'g'), jasmine.pp(arguments[i]));
  }
  return message;
}

// wraps format in a function so it can be set on this.message
function message() {
  var args = arguments;
  return function () {
    return format.apply(this, args);
  };
}

// Asserts whether the actual promise object resolves
function toResolve() {
  var success = jasmine.createSpy('Promise success callback');
  this.actual.then(success, noop);
  createScope().$digest();

  this.message = function () {
    return [
      'Expected promise to have been resolved',
      'Expected promise not to have been resolved'
    ];
  };

  return success.calls.length > 0;
}

/*
Asserts whether the actual promise object resolves with the given expected object, using angular.equals.
If expected is a method, it will assert whether the promise object was resolved, and execute the callback
with the response object, so that the spec can do its own assertions. Useful for more complex data.
*/
function toResolveWith(expected) {
  var success = jasmine.createSpy('Promise success callback');

  this.actual.then(success, noop);
  createScope().$digest();
  var actualResult = success.mostRecentCall.args[0];

  this.message = function () {
    return [
      'Expected promise to have been resolved with ' +  jasmine.pp(expected) + ' but was resolved with ' + jasmine.pp(actualResult),
      'Expected promise not to have been resolved with ' + jasmine.pp(expected) + ' but was resolved with ' + jasmine.pp(actualResult)
    ];
  };

  if (_.isFunction(expected)) {
    expected(success.mostRecentCall.args[0]);
    return success.calls.length > 0;
  } else {
    return angular.equals(actualResult, expected);
  }
}

// Asserts whether the actual promise object is rejected
function toReject() {
  var failure = jasmine.createSpy('Promise failure callback');

  this.actual.then(noop, failure);
  createScope().$digest();

  this.message = function () {
    return [
      'Expected promise to have been rejected',
      'Expected promise not to have been rejected'
    ];
  };

  return failure.calls.length > 0;
}

/*
Asserts whether the actual promise object rejects with the given expected object, using angular.equals.
If expected is a method, it will assert whether the promise object was rejected, and execute the callback
with the response object, so that the spec can do its own assertions. Useful for more complex data.
*/
function toRejectWith(expected) {
  var failure = jasmine.createSpy('Promise failure callback');

  this.actual.then(noop, failure);
  createScope().$digest();
  var actualResult = failure.mostRecentCall.args[0];

  this.message = function () {
    return [
      'Expected promise to have been rejected with ' +  jasmine.pp(expected) + ' but was rejected with ' + jasmine.pp(actualResult),
      'Expected promise not to have been rejected with ' + jasmine.pp(expected) + ' but was rejected with ' + jasmine.pp(actualResult)
    ];
  };

  if (_.isFunction(expected)) {
    expected(failure.mostRecentCall.args[0]);
    return failure.calls.length > 0;
  } else {
    return angular.equals(actualResult, expected);
  }
}

function toContainIsolateScope(values) {
  var cleanedScope = {};
  _.each(this.actual.isolateScope(), function (val, key) {
    if (key !== 'this' && key.charAt(0) !== '$') {
      cleanedScope[key] = val;
    }
  });
  this.message = message('Expected element isolated scope to contain {0} but got {1}', values, cleanedScope);
  return _.matches(values)(cleanedScope);
}

var matchers = {
  toResolve: toResolve,
  toResolveWith: toResolveWith,
  toHaveBeenResolved: toResolve,
  toHaveBeenResolvedWith: toResolveWith,
  toReject: toReject,
  toRejectWith: toRejectWith,
  toHaveBeenRejected: toReject,
  toHaveBeenRejectedWith: toRejectWith,
  toContainIsolateScope: toContainIsolateScope
};

beforeEach(function () {
  this.addMatchers(matchers);
});

jasmine.getJSONFixtures().fixturesPath = 'base/test/mock';