
module.exports = function (config) {
  require('./base.conf')(config);

  config.set({

    // list of files / patterns to load in the browser
    files: [
      'app/bower_components/jquery/dist/jquery.js',
      'app/bower_components/angular/angular.js',
      'app/bower_components/angular-mocks/angular-mocks.js',
      'app/bower_components/angular-resource/angular-resource.js',
      'app/bower_components/angular-route/angular-route.js',
      'app/bower_components/angular-sanitize/angular-sanitize.js',
      'app/bower_components/underscore/underscore.js',
      'app/scripts/**/*.js',
      'app/scripts/**/*.html',
      'test/spec/persons/**/*.js',
      'test/mock/**/*.json'
    ]
  });

};
