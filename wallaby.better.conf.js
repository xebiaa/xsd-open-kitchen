module.exports = function () {
  function n(file) { return { pattern: file, instrument: false }; }

  return {
    basePath: '../',
    debug: true,
    testFramework: 'jasmine@1.3.1',
    delays: {
      edit: 500
    },
    files: [
      // Libraries
      'app/bower_components/jquery/dist/jquery.js',
      'app/bower_components/angular/angular.js',
      'app/bower_components/angular-mocks/angular-mocks.js',
      'app/bower_components/angular-resource/angular-resource.js',
      'app/bower_components/angular-route/angular-route.js',
      'app/bower_components/angular-sanitize/angular-sanitize.js',
      'app/bower_components/underscore/underscore.js',

      // Scripts
      'app/scripts/**/*.js',
      'app/scripts/**/*.html',

      { pattern: 'test/mock/**/*.html', instrument: false, load: false },
      { pattern: 'test/mock/**/*.json', instrument: false, load: false }
    ],

    tests: [
      'test/spec/persons-better/**/*.js'
    ],

    preprocessors: {
      'app/scripts/**/*.html': function html2js(file) {
        return require('wallaby-ng-html2js-preprocessor').transform(file, {
          stripPrefix: 'app/'
        });
      }
    }
  };

};
