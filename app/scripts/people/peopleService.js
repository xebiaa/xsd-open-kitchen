angular.module('myApp')
  .factory('peopleService', function (PeopleResource) {
    return {
      getPeople: function () {
        return PeopleResource.query().$promise;
      },
      savePerson: function (person) {
        return person.$save();
      }
    };
  });
