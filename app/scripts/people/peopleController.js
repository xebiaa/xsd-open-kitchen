angular.module('myApp')
  .controller('PeopleController', function ($log, $scope, peopleService) {
    peopleService.getPeople()
      .then(function (people) {
        $scope.people = people;
      })
      .catch(function (error) {
        $log.log(error.data);
      });

    $scope.save = function (person) {
      peopleService.savePerson(person)
        .catch(function () {
          $log.log('Er ging iets mis met saven');
        });
    };
  });
