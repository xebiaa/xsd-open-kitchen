angular.module('myApp')
  .factory('PeopleResource', function ($resource) {
    return $resource('scripts/people/people.json');
  });
