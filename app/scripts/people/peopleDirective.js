angular.module('myApp').directive('peopleDirective', function () {
  return {
    scope: { people: '=peopleDirective' },
    template: '<ul>' +
              '<li ng-repeat="person in people"><a ng-href="#/{{person.route}}">{{person.first}}</a></li>' +
              '</ul>'
  };
});
